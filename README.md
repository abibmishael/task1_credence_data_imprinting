# Task Add Header,Footer and WaterMark to a given pdf file.

How to run the Application:- (Irrespective of Operatins System)
1. Download the application, and open the project in node.
2. Run the application with cmd - nodemon.
3. Got to browser - http://localhost:3000
4. Create a folder named /upload in the root path of the project.
5. Upload a pdf file, if the file is validated then it will be uploaded in upload folder.
6. Then header, footer and watermark will be added and it will generating as the same pdf.
7. Then You will be able to save the file with the changes to your local path.

Thank You

@Abib Mishael P
